#!/bin/sh

service ssh start
cat <<EOF >/home/user/start.sh
#!/bin/sh
{binary}
wait
exit 0
EOF
chown user:user /home/user/start.sh

su -c "xpra start :100 --no-mdns --no-pulseaudio --no-daemon --start-child='sh /home/user/start.sh'" user

