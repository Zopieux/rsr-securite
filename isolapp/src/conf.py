import os.path

ISOLAPP_ROOT = os.path.expanduser('~/.local/share/isolapp')
ISOLAPP_PREFIX = 'isolapp-img-'
TMP_BUILD = 'isolapp-build-%s-'
HOMES_ROOT = os.path.join(ISOLAPP_ROOT, 'homes')
SSHKEYS_ROOT = os.path.join(ISOLAPP_ROOT, 'keys')
