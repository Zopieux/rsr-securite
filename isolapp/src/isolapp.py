#!/usr/bin/env python

import argparse
import io
import json
import logging
import os
import select
import shlex
import shutil
import socket
import subprocess
import sys
import tempfile

import conf

ROOT = os.path.dirname(os.path.abspath(__file__))
logger = logging.getLogger('isolapp')


class DockerapiError(Exception):
    def __init__(self, code, cause):
        self.code = code
        self.cause = error


def _dockerapi(method, *args, **kwargs):
    try:
        cargs = [os.path.join(ROOT, 'dockerapi'), method]
        cargs.extend(args)
        return subprocess.Popen(
            cargs,
            stdout=subprocess.PIPE, stderr=subprocess.STDOUT, **kwargs
        )
    except subprocess.CalledProcessError as err:
        raise DockerapiError(proc.returncode, err)


def stop_container(container):
    try:
        _dockerapi("stop", container).wait()
        logger.debug("Stopped container %s", container)
    except DockerapiError as err:
        logger.warning("Container %s could not be stopped (already down?)", container)


def handle_isolate(opts, logger):
    opts.program = opts.program.lower()

    if opts.package_name is None:
        opts.package_name = opts.program

    opts.package_name = opts.package_name.lower()

    if opts.name is None:
        opts.name = opts.program

    opts.name = opts.name.lower()

    name = opts.name
    image_name = conf.ISOLAPP_PREFIX + name

    logger.info("Creating app %s for binary %s (primary package %s)", name, opts.program, opts.package_name)

    with open(os.path.join(ROOT, 'template', 'Dockerfile')) as df:
        dockerfile = df.read()

    with open(os.path.join(ROOT, 'template', 'start.sh')) as ssf:
        startshfile = ssf.read()

    packages = list(opts.package) if opts.package else []
    packages.insert(0, opts.package_name)
    dockerfile = dockerfile.format(
        packages=' '.join(shlex.quote(p) for p in packages),
        uid=os.geteuid(),
    )
    startshfile = startshfile.format(
        binary=shlex.quote(opts.program),
    )

    build_dir = tempfile.TemporaryDirectory(prefix=conf.TMP_BUILD % name)
    logger.debug("Building Docker image in %s", build_dir.name)

    with build_dir:
        with open(os.path.join(build_dir.name, 'start.sh'), 'w', encoding='utf8') as startf:
            startf.write(startshfile)

        with open(os.path.join(build_dir.name, 'Dockerfile'), 'w', encoding='utf8') as df:
            df.write(dockerfile)

        try:
            proc = _dockerapi('build', name, cwd=build_dir.name, universal_newlines=True, bufsize=1)
            while proc.poll() is None:
                data = proc.stdout.readline().strip()
                if data:
                    logger.debug(data)

        except DockerapiError as err:
            logger.error("Docker build failed.")
            sys.exit(1)

    home_dir = os.path.join(conf.HOMES_ROOT, name)
    ssh_dir = os.path.join(home_dir, '.ssh')
    logger.debug("Creating persistent home in %s", home_dir)
    try:
        os.makedirs(ssh_dir, mode=0o700)
    except OSError:
        logger.warning("Warning: persistent home %s already exists", home_dir)

    try:
        os.makedirs(conf.SSHKEYS_ROOT, mode=0o700)
    except OSError:
        pass

    key_path = os.path.join(conf.SSHKEYS_ROOT, name)

    # remove old key to prevent shitty ssh-keygen from asking to overwrite
    try:
        os.unlink(key_path)
        logger.debug("Old SSH key pair deleted")
    except OSError:
        pass

    logger.debug("Creating SSH key pair as %s", key_path)
    try:
        subprocess.call([
            'ssh-keygen', '-q', '-t', 'rsa', '-N', '',
            '-C', image_name, '-f', key_path,
            ], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    except subprocess.CalledProcessError:
        logger.error("ssh-keygen failed.")
        sys.exit(2)

    # put the pub key in authorized_keys
    authorized_keys_path = os.path.join(ssh_dir, 'authorized_keys')
    shutil.copy(key_path + '.pub', authorized_keys_path, follow_symlinks=False)
    os.chmod(authorized_keys_path, 0o600)


def handle_run(opts, logger):
    name = opts.name
    home_dir = os.path.join(conf.HOMES_ROOT, name)
    key_path = os.path.join(conf.SSHKEYS_ROOT, name)
    if not os.path.exists(key_path):
        logger.debug("No such file: %s", key_path)
        logger.error("%s is not a valid isolapp application name", name)
        sys.exit(1)

    try:
        proc = _dockerapi('run', name, shlex.quote(home_dir))
        proc.wait()
    except DockerapiError as err:
        logger.error("%s", proc.stderr.read().decode('utf8'))
        logger.error("docker run failed.")
        sys.exit(2)

    container = proc.stdout.readline().decode('ascii').strip()
    logger.debug("Container ID is %s", container)

    # get SSH port
    try:
        proc = _dockerapi('port', name)    
    except DockerapiError as err:
        logger.error("%s", proc.stderr.read().decode('utf8'))
        logger.error("docker port failed.")
        sys.exit(3)

    ssh_port = int(proc.stdout.readline().decode('ascii').strip().split(':', 1)[1])
    logger.debug("Local SSH port for 22/ssh is %d", ssh_port)

    logger.debug("Attaching xpra display")
    try:
        # FIXME: -o "StrictHostKeyChecking no" prevents SSH from starting (wtf)
        subprocess.check_call(['xpra', 'attach',
        #'--ssh=ssh -q -o "UserKnownHostsFile /dev/null" -o "StrictHostKeyChecking no" -i %s' % shlex.quote(key_path),
        '--ssh=ssh -i %s' % shlex.quote(key_path),
        '--no-tray', '--no-microphone', '--no-speaker', '--no-sharing',
        '--no-notifications', '--enable-pings',
        'ssh/user@localhost:%d/100' % ssh_port,
        ], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    except KeyboardInterrupt:
        logger.warning("ctrl+c caught")
    except subprocess.CalledProcessError as err:
        logger.error("xpra exited badly")
    finally:
        logger.warning("xpra exited; cleaning up")
        logger.debug("Shutdowning remote xpra")
        subprocess.check_call(['xpra', 'stop',
            '--ssh=ssh -i %s' % shlex.quote(key_path),
            'ssh/user@localhost:%d/100' % ssh_port,
            ], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        # cleanup
        logger.debug("Stopping container %s", name)
        stop_container(name)


def handle_delete(opts, logger):
    name = opts.name
    home_dir = os.path.join(conf.HOMES_ROOT, name)
    logger.debug("Stopping container %s", name)
    stop_container(name)
    logger.debug("Removing image %s", name)
    try:
        _dockerapi('rm', name)
    except DockerapiError as err:
        logger.warning("Error deleting image")
    ssh_key = os.path.join(conf.SSHKEYS_ROOT, name)
    try:
        os.unlink(ssh_key)
        logger.debug("Removed SSH key: %s", ssh_key)
    except OSError:
        logger.warning("SSH key did not exist: %s", ssh_key)
    if opts.prune:
        logger.debug("Pruning persistent data in: %s", home_dir)
        shutil.rmtree(home_dir)


def handle_stop(opts, logger):
    name = opts.name
    logger.debug("Stopping container %s", name)
    stop_container(name)


def handle_list(opts, logger):
    def query(what):
        output = _dockerapi('list', what).stdout.read()
        return json.loads(output.decode('utf8').split('\r\n\r\n', 1)[1])

    containers = query('containers')
    running = set()
    for cont in containers:
        try:
            empty, prefix, name = cont['Image'].split(':', 1)[0].partition(conf.ISOLAPP_PREFIX)
            if empty or prefix != conf.ISOLAPP_PREFIX:
                raise ValueError()
        except ValueError:
            continue
        running.add(name)

    images = query('images')
    avail_images = {}
    for image in images:
        for tag in image.get('RepoTags', []):
            try:
                empty, prefix, name = tag.split(':', 1)[0].partition(conf.ISOLAPP_PREFIX)
                if empty or prefix != conf.ISOLAPP_PREFIX:
                    raise ValueError()
            except ValueError:
                continue
            if os.path.exists(os.path.join(conf.HOMES_ROOT, name)):
                avail_images[name] = name in running

    print("{:>20}  {}".format("NAME", "STATUS"))
    for name, running in sorted(list(avail_images.items())):
        print("{:>20}  {}".format(name, "running" if running else "dead"))
    

def main():
    parser = argparse.ArgumentParser(description="isolapp command-line interface")
    parser.add_argument('-v', '--verbose', dest='verbose', action='store_true',
        help="verbose output")

    subparsers = parser.add_subparsers(dest='command')
    parser_isolate = subparsers.add_parser('isolate', help="isolate a new app")
    parser_isolate.add_argument('package_name', nargs='?',
        metavar="PACKAGE-NAME")
    parser_isolate.add_argument('program', metavar="PROGRAM")
    parser_isolate.add_argument('-n', '--name', required=False)
    parser_isolate.add_argument('-p', '--package', action='append', 
        metavar='MORE-PACKAGES', help="Other packages to install")

    parser_run = subparsers.add_parser('run', help="run an isolated app")
    parser_run.add_argument('name', help="name of the app to run")

    parser_stop = subparsers.add_parser('stop', help="stop a running isolated app")
    parser_stop.add_argument('name', help="name of the app to stop")

    parser_list = subparsers.add_parser('list', help="list available isolated apps")

    parser_del = subparsers.add_parser('delete', help="delete an isolated app")
    parser_del.add_argument('name', help="name of the app to delete")
    parser_del.add_argument('-p', '--prune', action='store_true',
        help="prune all user data (virtual home directory)")

    if os.geteuid() == 0:
        parser.error("This program must NOT be run as root.")

    args = parser.parse_args()

    if not args.command:
        parser.error("You must provide an action.")

    logging.basicConfig(
        format="[%(name)s] %(message)s",
        level=logging.DEBUG if args.verbose else logging.WARNING
    )

    command = globals()['handle_%s' % args.command]
    logger.info("Running `%s` command", args.command)

    sublogger = logging.getLogger('%s.%s' % (logger.name, args.command))
    command(args, sublogger)


if __name__ == '__main__':
    main()
