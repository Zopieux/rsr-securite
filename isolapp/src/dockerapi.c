#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <errno.h>
#include <string.h>

#define DOCKER_SOCK_PATH "/var/run/docker.sock"
#define DOCKER "/usr/bin/docker"
#define SSH_PORT "22/tcp"
#define ISOLAPP_TPL "isolapp-img-%s"
#define STOP_TIMEOUT "4"
#define NULLC ((char *)0)
#define BUFFLEN 512

static uid_t euid, ruid;

static void err(char *msg)
{
    fprintf(stderr, msg);
    exit(-100);
}

/* Restore the effective UID to its original value. */
static void do_setuid(void)
{
    int status;

#ifdef _POSIX_SAVED_IDS
    status = seteuid (euid);
#else
    status = setreuid (ruid, euid);
#endif
    if (status < 0) {
        fprintf(stderr, "Couldn't set uid.\n");
        exit(status);
    }
}

/* Set the effective UID to the real UID. */
static void undo_setuid(void)
{
    int status;

#ifdef _POSIX_SAVED_IDS
    status = seteuid(ruid);
#else
    status = setreuid(euid, ruid);
#endif
    if (status < 0) {
        fprintf (stderr, "Couldn't set uid.\n");
        exit(status);
    }
}

static int isolapp_name(char *dest, const char *name, const char* varname)
{
    if (snprintf(dest, BUFFLEN, ISOLAPP_TPL, name) >= BUFFLEN) {
        fprintf(stderr, "%s is too long\n", varname);
        return 0;
    }
    return 1;
}

static int exec_cmd(char **cmd)
{
    int ret, r;
    int pipefd[2];
    char buff[BUFFLEN];
    pid_t pid = 0;

    pipe(pipefd);

    pid = fork();
    if (pid == 0) {
        // child
        close(pipefd[0]);
        dup2(pipefd[1], STDOUT_FILENO);
        dup2(pipefd[1], STDERR_FILENO);
        do_setuid();
        execv(DOCKER, cmd);
    }

    close(pipefd[1]);

    while ((r = read(pipefd[0], buff, BUFFLEN)) > 0)
        write(STDOUT_FILENO, buff, r);

    waitpid(pid, &ret, 0);
    return WEXITSTATUS(ret);
}

static int docker_query(const char *path)
{
    char buff[BUFFLEN];
    struct sockaddr_un remote;
    int s, len, nread;

    do_setuid();

    if ((s = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
        perror("socket");
        return 10;
    }

    remote.sun_family = AF_UNIX;
    strcpy(remote.sun_path, DOCKER_SOCK_PATH);
    len = strlen(remote.sun_path) + sizeof(remote.sun_family);
    if (connect(s, (struct sockaddr *)&remote, len) == -1) {
        perror("connect");
        return 11;
    }

    undo_setuid();

    len = snprintf(buff, BUFFLEN, "GET %s HTTP/1.0\r\n\r\n", path);
    if (len >= BUFFLEN) {
        fprintf(stderr, "path too long\n");
        return 12;
    }
    if (send(s, buff, strlen(buff), 0) == -1) {
        perror("send");
        return 13;
    }
    while ((nread = recv(s, buff, BUFFLEN, 0)) > 0) {
        if (nread <= 0)
            break;
        buff[nread] = '\0';
        fwrite(buff, nread, sizeof(char), stdout);
    }
    close(s);
    return 0;
}

static int docker_list(int images)
{
    return docker_query(images ? "/v1.16/images/json" : "/v1.16/containers/json");
}

static int docker_build(const char *name)
{
    char fname[BUFFLEN];
    if (!isolapp_name(fname, name, "name"))
        return -10;

    char *cmd[] = {DOCKER, "build", "--rm", "-t", fname, ".", NULLC};
    return exec_cmd(cmd);
}

static int docker_run(const char *name, const char *home)
{
    char buffn[BUFFLEN], buffv[BUFFLEN], fname[BUFFLEN];

    if (!isolapp_name(fname, name, "name"))
        return -10;

    if (snprintf(buffn, BUFFLEN, "--name=%s", fname) >= BUFFLEN) {
        fprintf(stderr, "name is too long\n");
        return -11;
    }
    if (snprintf(buffv, BUFFLEN, "%s:/home/user", home) >= BUFFLEN) {
        fprintf(stderr, "home is too long\n");
        return -12;
    }

    char *cmd[] = {DOCKER, "run", "-t", "-i", "--detach", "-p", "22", buffn, "-v", buffv, fname, NULLC};
    return exec_cmd(cmd);
}

static int docker_port(const char *name)
{
    char fname[BUFFLEN];
    if (!isolapp_name(fname, name, "name"))
        return -10;
    char *cmd[] = {DOCKER, "port", fname, SSH_PORT, NULLC};
    return exec_cmd(cmd);
}

static int docker_stop(const char *name)
{
    int ret;
    char fname[BUFFLEN];
    if (!isolapp_name(fname, name, "name"))
        return -10;
    char *cmds[] = {DOCKER, "stop", "-t", STOP_TIMEOUT, fname, NULLC};
    char *cmdr[] = {DOCKER, "rm", fname, NULLC};
    ret = exec_cmd(cmds);
    ret |= exec_cmd(cmdr);
    return ret;
}

static int docker_rm(const char *name)
{
    char fname[BUFFLEN];
    if (!isolapp_name(fname, name, "name"))
        return -10;
    char *cmd[] = {DOCKER, "rmi", "-f", fname, NULLC};
    return exec_cmd(cmd);
}

int main(int argc, char** argv)
{
    if (argc <= 1) {
        fprintf(stderr, "Usage: %s (list|build|run|port|stop|rm) [args...]\n", argv[0]);
        return -1;
    }

    int ret;

    if (!strncmp("list", argv[1], 5)) {

        if (argc <= 2) {
type_arg:
            err("list needs <type> argument: 'containers' or 'images'");
        }
        if (!strcmp("containers", argv[2])) {
            ret = docker_list(0);
        } else if (!strcmp("images", argv[2])) {
            ret = docker_list(1);
        } else {
            goto type_arg;
        }

    } else if (!strncmp("build", argv[1], 5)) {

        if (argc <= 2) {
            err("build needs <image name> argument\n");
        }

        ret = docker_build(argv[2]);

    } else if (!strncmp("run", argv[1], 5)) {

        if (argc <= 3) {
            err("run needs <name> <home> arguments\n");
        }

        ret = docker_run(argv[2], argv[3]);

    } else if (!strncmp("port", argv[1], 4)) {

        if (argc <= 2) {
            err("port needs <name> argument\n");
        }

        ret = docker_port(argv[2]);

    } else if (!strncmp("stop", argv[1], 4)) {

        if (argc <= 2) {
            err("stop needs <name> argument\n");
        }

        ret = docker_stop(argv[2]);

    } else if (!strncmp("rm", argv[1], 2)) {

        if (argc <= 2) {
            err("rm needs <name> argument\n");
        }

        ret = docker_rm(argv[2]);

    } else {

        fprintf(stderr, "Unknown command: %s\n", argv[1]);
        return -2;
    }

    return ret;
}
