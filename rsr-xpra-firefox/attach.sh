#!/bin/sh

SSH_ROOT=$1
if [ -z $SSH_ROOT ]; then
	echo "Usage: $0 <ssh key root path>" >&2
	exit 1
fi

xpra attach --ssh="ssh -i $SSH_ROOT/id_rsa" ssh/user@localhost:2022/100
