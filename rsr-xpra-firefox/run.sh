#!/bin/sh

if [ $(id -u) -ne 0 ]; then
	echo "Must be run as root" >&2
	exit 2
fi

HOMES_ROOT=$1
if [ -z $1 ]; then
	echo "Usage: $0 <homes root path>" >&2
	exit 1
fi

docker run -ti --rm -p 2022:22 -v "$HOMES_ROOT/firefox":/home/user rsr-xpra-firefox

