#!/bin/bash

DISPLAY=":1"
RESOLUTION="1024x768x24"

export DISPLAY
Xvfb "$DISPLAY" -extension GLX -screen 0 "$RESOLUTION" &
/usr/bin/openbox-session &
sleep 1
xterm &
x11vnc -forever -usepw -create -nobell -noxdamage -nap -display "$DISPLAY"

